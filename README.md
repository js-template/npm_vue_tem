# 基于vue发布npm包的项目模板架构说明文件

## 目录说明
```
examples // 本地开发查看包效果的演示目录  

lib // 此npm包打包后

packages // npm包开发目录
    todoList // todoList包
        src // 主要功能实现的.vue文件
        index.js // 对外提供对该npm包文件的引用

public // 静态资源目录

tests // 单元测试  

.eslintrc.js // eslint配置

.gitignore // git忽略文件

.npmignore // npm发包的忽略文件

项目模板架构说明文件.md // 该项目架构相关说明

babel.config.js // babel配置文件

jesit.config.js // 单元测试框架jest配置文件

package.json 

README.md // 此npm包使用说明

```
---

## 如何开发一个npm包

### 新建相关目录和文件
在packages目录下新建你准备开发包的名称代号（如demo1)，参考todoList相关新建对应文件，做对应修改 

### 修改package.json里的相关信息
默认是wd-todolist-vue包相关的，可根据实际修改（遵守相关命名规则：公司组织代号-包名简称-框架）  

参考：https://docs.npmjs.com/creating-a-package-json-file

### 本地跑起服务，查看该包效果
npm run dev  

### 增加演示代码，辅助本地开发
在examples/App.vue里引入该组件（参考todolist组件）

### 增加单元测试
项目默认使用的jest单元测试框架。在tests/unit目录下新建相关单元测试文件，并编写测试用例

### 编写该npm包使用说明
在README.md里编写该npm包使用说明

### 删除示例，避免打不必要的包
删除示例的packages/todoList以及App.vue里的相关引入

### 提交代码
#### 代码通过gitlab管理
- 登录gitlab，访问http://192.168.0.223:9090/architecture/front-end/npm （无权限，联系leonard）

- 新建相关git仓库（名称和该npm包名一致，遵守相关命名规则）

- 提交并推送代码到此仓库

---

### 发布npm包
npm publish

### 更新版本
先切换到master分支  
每次修改完组件后，我们需要更新版本 

#### 方式一 修改 package.json 的version版本
规则：对于"version":"x.y.z"  
1.修复bug,小改动，增加z  
2.增加了新特性，但仍能向后兼容，增加y  
3.有很大的改动，无法向后兼容,增加x  
然后重新npm publish发布包  


#### 方式二 使用命令修改版本

使用命令：npm version <update_type>进行修改，update_type 有三个参数，  

第一个是patch, 第二个是minor,第三个是 major，  

patch：这个是补丁的意思；  

minor：这个是小修小改；  

major：这个是大改动；  


具体咋用：

比如我想来个1.0.1版本，注意，是最后一位修改了增1，那么命令：npm version patch 回车就可以了；

比如我想来个1.1.0版本，注意，是第二位修改了增1，那么命令： npm version minor 回车就可以了；

比如我想来个2.0.0版本，注意，是第一位修改了增1，那么命令： npm version major 回车就可以了；

然后 使用 npm publish 发布即可

---

### 取消已发布的npm包 

#### 终端cli命令取消
参考： https://docs.npmjs.com/unpublishing-packages-from-the-registry
##### 取消发布软件包的单个版本
要取消发布软件包的单个版本，请运行以下命令，用package-name软件包的名称和version版本号替换： 

npm unpublish package-name@version

#####  取消发布整个程序包
要取消发布整个程序包，请运行以下命令，用package-name您的程序包名称替换：

npm unpublish package-name -f  
如果为写入启用了双重身份验证，则需要在unpublish命令中添加一次性密码--otp=123456（其中123456是您的身份验证器应用程序中的代码）。

####  私有npm仓库删除直接在服务器上删除
一般情况建议用终端单个删除。如果是想一次性删除多个的话，连接上你部署verdaccios的服务器，具体操作示例如下：  

cd /home/yg/.config/verdaccio  
~/.config/verdaccio$ ls  
config.yaml  htpasswd  storage  
~/.config/verdaccio$ cd storage  
~/.config/verdaccio/storage$ ls  
~/.config/verdaccio/storage$ rm -rf  包名  
