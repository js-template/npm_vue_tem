/*
 * @Description: TODOLIST 相关单元测试文件
 * @Date: 2020-03-02 13:52:07
 * @LastEditTime: 2020-03-10 21:21:31
 */
import { shallowMount, mount } from '@vue/test-utils';
import TodoIndex from './../../packages/todoList/src/index.vue';
import TodoHead from './../../packages/todoList/src/TodoHead.vue';
import TodoLists from './../../packages/todoList/src/TodoLists.vue';
import TodoItem from './../../packages/todoList/src/TodoItem.vue';
import TodoFooter from './../../packages/todoList/src/TodoFooter.vue';

describe('todo index.vue', () => {
    describe('TodoIndex组件依赖的相关数据正常', () => {
        const wrapper = shallowMount(TodoIndex);
        it('data里的初始数据', () => {
            expect(wrapper.vm.lists).toStrictEqual([
                { id: 1, tit: '吃饭', done: true },
                { id: 2, tit: '睡觉', done: true },
                { id: 3, tit: '打豆豆', done: false },
            ]);

            expect(wrapper.vm.showType).toBe(0);
        });

        it('computed输出的数据', () => {
            expect(Array.isArray(wrapper.vm.curMapLists)).toBeTruthy();
        });
    });

    describe('methods里相关方法', () => {
        it('setItemDat', () => {
            const wrapper = shallowMount(TodoIndex);
            expect(typeof wrapper.vm.setItemDat).toBe('function');
            wrapper.vm.setItemDat({ id: 3, tit: '33333', done: false });
            expect(wrapper.vm.lists.find(item => item.id === 3).tit).toBe('33333');
        });

        it('addItem', () => {
            const wrapper = shallowMount(TodoIndex);
            expect(typeof wrapper.vm.addItem).toBe('function');
            wrapper.vm.addItem('1111111111');
            expect(wrapper.vm.lists.find(item => item.tit === '1111111111')).toBeTruthy();
        });

        it('delItem', () => {
            const wrapper = shallowMount(TodoIndex);
            expect(typeof wrapper.vm.delItem).toBe('function');
            wrapper.vm.delItem(3);
            expect(wrapper.vm.lists.find(item => item.id === 3)).not.toBeTruthy();
        });
    });

    describe('TodoIndex局部components', () => {
        expect(TodoIndex.components.TodoHead).toBeTruthy();
        expect(TodoIndex.components.TodoLists).toBeTruthy();
        expect(TodoIndex.components.TodoFooter).toBeTruthy();
    });

    describe('TodoIndex的子组件相关触发事件', () => {
        const wrapper = shallowMount(TodoIndex, {
            components: {
                // 由于子组件是懒加载的所以单元测试时得挂载时就注入下
                TodoHead,
                TodoLists,
                TodoFooter,
            },
        });
        it('TodoIndex>TodoHead', () => {
            wrapper.find(TodoHead).vm.$emit('add', '6666666666');
            expect(wrapper.vm.lists.find(item => item.tit === '6666666666')).toBeTruthy();
        });

        it('TodoIndex>TodoLists', () => {
            const TodoListsCom = wrapper.find(TodoLists);

            const changeObj = { id: 3, tit: 1, done: true };
            TodoListsCom.vm.$emit('changeItemTit', changeObj);
            expect(wrapper.vm.lists.find(item => item.id === 3)).toBe(changeObj);

            TodoListsCom.vm.$emit('del', 3);
            expect(wrapper.vm.lists.find(item => item.id === 3)).toBeFalsy();
        });

        it('TodoIndex>TodoFooter', () => {
            wrapper.find(TodoFooter).vm.$emit('update:typeVal', 1);
            expect(wrapper.vm.showType).toBe(1);
            expect(wrapper.vm.curMapLists.some(item => item.done)).toBe(true);
        });
    });
});

describe('todo TodoHead.vue', () => {
    describe('TodoHead data', () => {
        const wrapper = shallowMount(TodoHead);
        expect(wrapper.vm.tit).toBe('');

        wrapper.setData({ tit: 111 });
        expect(wrapper.vm.tit).toBe(111);
    });

    describe('TodoHead methods', () => {
        const wrapper = shallowMount(TodoHead);
        wrapper.vm.addFun();
        expect(wrapper.vm.tit).toBe('');
    });
});

describe('todo TodoLists', () => {
    describe('TodoLists init data', () => {
        const wrapper = shallowMount(TodoLists);
        expect(wrapper.vm.lists).toStrictEqual([]);

        const dat = { id: 11, tit: 11, done: false };
        wrapper.setProps({ lists: [dat] });
        expect(wrapper.vm.lists).toStrictEqual([dat]);
    });

    describe('TodoLists>components', () => {
        const wrapper = mount(TodoLists, {
            components: { TodoItem },
        });
        expect(wrapper.vm.$options.components.TodoItem).toBeTruthy();

        /* // 有问题。。
        const TodoItemCom = wrapper.find(TodoItem);
        console.log('TodoItemCom: ', TodoItemCom);
        TodoItemCom.vm.$emit('delItem');
        expect(TodoItemCom.emitted().delItem).toBeTruthy();

        TodoItemCom.vm.$emit('changeItemTit');
        expect(TodoItemCom.emitted().changeItemTit).toBeTruthy(); */
    });
});

describe('todo TodoLists>TodoItem', () => {
    it('TodoItem init data', () => {
        const wrapper = shallowMount(TodoItem, {
            propsData: {
                dat: { id: 6, tit: 6, done: true },
            },
        });
        expect(wrapper.vm.dat).toStrictEqual({ id: 6, tit: 6, done: true });

        expect(wrapper.vm.isSetTit).toBe(false);
    });

    it('TodoItem methods', done => {
        const wrapper = shallowMount(TodoItem);
        wrapper.vm.isSetTit = true;
        wrapper.vm.$nextTick(() => {
            wrapper.vm.changeTit();
            expect(wrapper.vm.isSetTit).toBeFalsy();
            done();
        });
    });
});

describe('todo TodoFooter', () => {
    it('TodoFooter init data', () => {
        const wrapper = shallowMount(TodoFooter);
        expect(wrapper.vm.typeVal).toBe(0);

        wrapper.setProps({ typeVal: -1 });
        expect(wrapper.vm.typeVal).toBe(-1);
    });
});
