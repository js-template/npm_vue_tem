/*
 * @Description: DemoCom组件出口
 * @Date: 2020-03-08 14:46:35
 * @LastEditTime: 2020-03-08 17:54:38
 */
import TodoList from './src/index.vue';
import TodoFooter from './src/TodoFooter.vue';
import TodoHead from './src/TodoHead.vue';
import TodoItem from './src/TodoItem.vue';
import TodoLists from './src/TodoLists.vue';

// 为组件提供 install 安装方法，供按需引入
TodoList.install = function(Vue) {
    Vue.component('TodoFooter', TodoFooter);
    Vue.component('TodoHead', TodoHead);
    Vue.component('TodoItem', TodoItem);
    Vue.component('TodoLists', TodoLists);
    Vue.component('TodoList', TodoList);
};

// 默认导出组件
export default TodoList;
