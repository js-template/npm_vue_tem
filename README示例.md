[English](README.md) | 简体中文

# md 文档编写示例（具体按实际对应修改）

[![Build Status](https://travis-ci.org/simbawus/digital-keyboard.svg?branch=master)](https://travis-ci.org/simbawus/digital-keyboard)
[![Coverage Status](https://s3.amazonaws.com/assets.coveralls.io/badges/coveralls_50.svg)](https://coveralls.io/github/simbawus/digital-keyboard?branch=master)
[![npm](https://img.shields.io/npm/v/digital-keyboard.svg)](https://www.npmjs.com/package/digital-keyboard)
[![npm](https://img.shields.io/npm/dt/digital-keyboard.svg)](https://www.npmjs.com/package/digital-keyboard)
[![GitHub license](https://img.shields.io/github/license/simbawus/digital-keyboard.svg)](https://github.com/simbawus/digital-keyboard/blob/master/LICENSE)

-   API 简洁，非常好上手

## 属性

| Property   | Type   | Default                                                                                                                                            | Description              |
| :--------- | :----- | :------------------------------------------------------------------------------------------------------------------------------------------------- | :----------------------- |
| urlObject  | Object | url 上的参数                                                                                                                                       | url 上的参数             |
| domain     | String |                                                                                                                                                    | 展示回调请求的域名       |
| paramsList | Array  | test,oid,brobdl,model,network,t,aid,gaid,pbl,p,appkey,scene,geo,ver,sver,source,utm_source,cbType,rdv,a1,b1,c1,d1,e1,f1,g1,h1,i1,j1,k1,l1,m1,n1,o1 | 页面跳转时保留的参数列表 |

## 方法

-   `imp(domain)`: 展示回调
-   `jump(url,params)`: 保留参数页面跳转
-   `open(url,params)`: 保留参数打开新窗口

## 开始上手

### 安装

#### yarn

```shell
yarn add H5SDK
```

#### npm

```shell
npm install H5SDK
```

### 使用示例

-   **原生 JavaScript**

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
    </head>
    <body>
        <script src="./index.min.js"></script>
        <script>
            var H5_SDK = new H5SDK();
            //展示回调
            H5_SDK.imp();

            //带参数展示回调
            H5_SDK.imp('\/\/www.baidu.com');

            //保留url参数页面跳转
            H5_SDK.jump('\/\/www.baidu.com');

            //保留url参数打开新窗口
            H5_SDK.open('\/\/www.baidu.com');
        </script>
    </body>
</html>
```

## 如何贡献

欢迎每个人为这个项目做出贡献。可以从查看我们[未解决的问题](https://www.baidu.com)、[提交新问题](https://www.baidu.com)或[提出新功能](https://www.baidu.com)入手，参与讨论投票您喜欢或不喜欢的问题。

## 开源证书

[**The MIT License**](LICENSE).
